#include <stdio.h>

char word[] = "rotator";

bool palindrome()
{
    int i, n;
    for (i = 0, n = sizeof(word - 1) - 1; i <= n; i++, n--) {
        if (word[i] != word[n]) return false;
    }
    return true;
}

int main()
{
    printf("%s is%s a palindrome\n", word, palindrome() ? "" : " not");
    return 0;
}