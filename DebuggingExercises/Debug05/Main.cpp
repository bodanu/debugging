#include <stdio.h>

enum class spring_thing
{
	bird
	, flower
	, baseball
	, golf
};

void print_sound(spring_thing x)
{
	switch (x) {
	case spring_thing::bird:
		printf("chirp chirp");
	case spring_thing::flower:
		printf("mum's the word");
	case spring_thing::baseball:
		printf("play ball");
	case spring_thing::golf:
		printf("$#*?&");
	default:
		return;
	}
	printf("\n");
}

int main()
{
	print_sound(spring_thing::bird);
	print_sound(spring_thing::flower);
	print_sound(spring_thing::baseball);
	print_sound(spring_thing::golf);
	return 0;
}