enum tea
{
	cinnamon = 1
	, ginseng = 2
	, ginger = 4
	, goji = 8
	, anise = 0x010
	, licorice = 0x020
	, kava = 0x040
	, clover = 0x080
	, hawthorn = 0x100
	, tulsi = 0x200
};
#include <stdio.h>

int main()
{
	int nX = 10;
	unsigned int nY = 15u;

	if (nX - nY < 0)
	{
		printf("Numarul este negativ (%u) \n", (nX - nY));
	}
	printf("Numarul este pozitiv (%u)\n", (nX - nY));
}