#include <iostream>
#include <Windows.h>

#define Print(x) std::cout<<x<<std::endl;

bool f2(double param)
{
	return param > 10;
}

void f1(double param)
{
	while (f2(param))
		param /= 2;
}

struct Entity
{
	int x, y;
};

/*
Debugging toolbar

Debug -> Windows
Hover mouse and pin
Change values on the fly

Set next statement (drag instruction pointer)
Breakpoints
  Simple & Conditional
  Hit count
  Function breakpoint (Debug -> New Breakpoint)
  Data breakpoint (Debug -> New Breakpoint)
  Filter (thread)
  Tracepoints(Breakpoint Action),
	value of a variable {i}
	$ADDRESS (Current instruction),
	$CALLER, $CALLSTACK,
	$FUNCTION (Current function name),
	$TID (Thread id),
	$TNAME (thread name),
	$TICK (Tick count (from Windows GetTickCount)
  Label breakpoints
Watch
Locals (current scope)
Autos (previous and current statement)
Memory
*/

int main()
{
	int a = 8;

	for (int i = 0; i < 10; ++i)
	{
		a += 1;
	}

	f1(100);

	int b = 10;

	{
		int b = 0;
		b += 5;
		Print(b);
	}

	Print(b);

	Entity e;
	e.x = 10;

	int* ptr = new int;
	*ptr = 20;

	{
		int* arr = new int[5];
		arr[0] = 1;
		arr[1] = 2;
		arr[2] = 3;
		arr[3] = 4;
		arr[4] = 5;
	}

	//Sleep(20000);
	// if the application has no debugger, it will display this string
	OutputDebugString((LPCWSTR)"Test");

	return 0;
}